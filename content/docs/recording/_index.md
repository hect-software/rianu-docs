---
weight: 20
draft: true

#bookFlatSection: true
---
# Recording
---

This software does not have any specific recording setup, as long as there is movement in the video it can most likely be tracked. We recommend a recording of at least 1980x1080 at 30FPS; however, this is just a suggestion and other resolutions and frame rates can still produce good results. 

In order to help with the recording process this page links some papers that show hECT recording platforms in use:


---
