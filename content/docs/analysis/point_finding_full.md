---
weight: 1
draft: true
---
# Point Finding
---

## **Data Processing**
### _**Data Smoothing**_
A savitzky–golay filter, as supplied by the `scipy` library, is
applied to the raw displacement data in order to smooth it enough for
analysis.  

```python
smoothed = savgol_filter(self.raw_disp, self.window, self.poly)
```

### _**Displacement Normalized**_
The data is supplied from tracking the distance between the centroids
of the two posts. To get proper force analysis we need the
displacement of the posts from their starting positions. In order to
achieve this the distance between the posts at every point is time is
subtracted from the starting distance of the posts. 
```python
self.smooth_disp = self.post_dist - smoothed
```

---
## **Peaks**  
The `peakutils` library is next implemented to find the peaks of the
data. The first and last peak are discarded to avoid the risk of going
off the edge of the dataset.  

```python
peakutils.indexes(self.smooth_disp, self.thresh, self.min_dist)[1:-1]
```


---
## **Basepoints**  
The basepoints are the frontpoints are found using the same function
just going in different directions from the peaks.
```python
def find_basepoints_frontpoints(self):
        """Use the dfdt_recursive func to find basepoints and frontpointspyl"""
        peak_indicies = self.peaks[2]
        basepoints = [self.dfdt_recursive(
            peak_index, lambda x:x-1) for peak_index in peak_indicies]
        frontpoints = [self.dfdt_recursive(
            peak_index, lambda x:x+1) for peak_index in peak_indicies]

        self.basepoints = self.format_points(basepoints)
        self.frontpoints = self.format_points(frontpoints)

def dfdt_recursive(self, peak_index, incrementor):
    """Recursively moves along graph until it changes direction"""
    new_index = incrementor(peak_index)
    if self.smooth_disp[new_index] - self.smooth_disp[peak_index] > 0:
        return peak_index
    return self.dfdt_recursive(new_index, incrementor)
```

---
## **Contractpoints** 

---
## **Relaxpoints**

---
## Variables
The variables used on this page are defined here. 

`self.raw` = The distance between the two posts of a
tissue. One data point for each frame.  
`self.window` = Higher is smoother but more biased.   
`self.poly` = The polynomial value the data is fitted to.   
`self.smoothed` = `self.raw` after being passed through a sav-gol
filter.  
`self.post_disp` = The starting distance between two posts.  
`self.smooth_disp` = `self.smoothed` after being normalized to
starting post distance.

---