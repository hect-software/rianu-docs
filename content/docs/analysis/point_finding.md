---
weight: 1

---
# Point Finding
---

{{< hint info >}}
**Accessibility**  
Code snippets will be populated. 
{{< /hint >}}

## **Data Processing**
### _**Data Smoothing**_
A savitzky–golay filter, as supplied by the `scipy` library, is
applied to the raw displacement data in order to smooth it enough for
analysis.  

### _**Displacement Normalized**_
The data is supplied from tracking the distance between the centroids
of the two posts. To get proper force analysis we need the
displacement of the posts from their starting positions. In order to
achieve this the distance between the posts at every point is time is
subtracted from the starting distance of the posts. 

---
## **Peaks**  
The `peakutils` library is next implemented to find the peaks of the
data. The first and last peak are discarded to avoid the risk of going
off the edge of the dataset. 

---
## **Basepoints**  
The basepoints are the frontpoints are found using the same function
just going in different directions from the peaks.

---
## **Contractpoints** 

---
## **Relaxpoints**

---
## Variables
The variables used on this page are defined here. 

---