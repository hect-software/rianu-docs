# Calculations
---
{{< hint info >}}
**Markdown content**  
Lorem markdownum insigne. Olympo signis Delphis! Retexi Nereius nova develat
stringit, frustra Saturnius uteroque inter! Oculis non ritibus Telethusa
{{< /hint >}}

---
## **Force Calculations**
### Cantilever Beam Systems
{{< katex display>}}
F(\delta) = \frac{3 \pi E R^4}{2a^2 (3L - a)} * \delta
{{< /katex >}}  

---

## **Time Calculations**

### **Beating Frequency**


Function accepts a list containing the timepoints of each peak. It finds the difference
between each peak time. Averages it and gets a standard deviation. 

---