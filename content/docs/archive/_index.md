---
weight: 45
#bookFlatSection: true
---

# Archive

---

Rianú allows the download of all experiment and bio reactor information for long term storage. These files can be uploaded to Rianú later.

---

## **Download Experiments**

Using the download button next to an experiment will download a zip file with all the information for that database including the videos within that experiment and the Bio Reactors used for the experiment.

Within the downloaded zip file there will be:

1. A csvs folder: within this folder is the tracking data for each tissue in the experiment.

   - CSV naming convention: \<tissue-number\>\_\<tissue-type\>\_\<tissue-id\>.csv

2. A videos folder: within this folder is each video that was uploaded as part of the experiment.

   - Video naming convention: \<date>\_\<frequency\>\_\<bio-reactor-number\>\_\<video-id\>.\<extension\>

3. A json file with data associated with the experiment.

---

## **Download Bio Reactors**

Downloading the bio reactors will yield a json file with all information about the bio reactors.

---

## **Upload Experiment**

The downloaded experiment zip file generated from downloading an experiment can be uploaded, this will repopulate the database with the experiment data and videos from the archive.

{{< hint danger >}}
It is important to note that if data over laps between the archive and the current database the information in the current database will be replaced with the information from the archive.
{{</hint>}}

## **Upload Bio Reactors**

The JSON file generated from downloading the Bio reactors can be uploaded to restore the bio reactors in the archive to the current database.

{{< hint danger >}}
It is important to note that if data over laps between the archive and the current database the information in the current database will be replaced with the information from the archive.
{{</hint>}}
