---
weight: 10
#bookFlatSection: true
---

## Currently Supported Systems
[`Costa Lab Multi-tissue`](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC6561092/)  
[`Costa Lab Single Tissue`](https://pubmed.ncbi.nlm.nih.gov/24174427/)     


---
## Add a bioreactor  
If you are a member of a lab that would like to use this software but
your bioreactor requires different force calculations please open an
issue at [`hECT-Software/rianu`](https://gitlab.com/hECT-Software/rianu) or send
an email to `jack@mrph.dev` and we can work to add support. 

---