---
weight: 10
#bookFlatSection: true
---
# Quick Start
---

## Installation

This software can be deployed on any machine, as long as Docker is
pre-installed, including a cloud server. 

### Local Computer Deployment 

If you primarily use one computer for analysis this is the preferred method. 

1. Make sure docker and docker compose are installed
2. (Optional) Install Docker desktop, very useful for managing images
3. Run the following command to get the docker compose file
    ```sh
    wget https://gitlab.com/hect-software/rianu/-/raw/main/docker-compose.yml
    ```
4. Run the following command in the same directory
    ```sh
    docker compose up -d
    ```
5. Rianú can be accessed via a browser at `http://localhost/`
6. (Optional) Use Docker Desktop app to stop and start the software whenever you wish.

### Cloud Deployment

Cloud deployment has been tested on a Digital Ocean droplet, but any web hosting
platform can be used if you have a particular preference. If you have
said preference I will assume you can modify the instructions below to
work for you, if you have never set up a remote server before then
just follow the instructions below. 

1. Create an account on [Digital Ocean](https://digitalocean.com).
2. Create a new droplet. 
3. For image select Docker from the Marketplace tab. 
4. For plan select any, all will work but higher cores will be faster.
5. Under additional options select `User data`. Copy and past the
   following bash script.
    ```sh
    #!/bin/bash
    wget https://gitlab.com/hect-software/rianu/-/raw/main/docker-compose.yml -P /root/
    docker compose -f /root/docker-compose.yml up -d
    ```
6. Set authentication. Preferably with SSH keys however if you don't
   know what those are just set a password. These will only be used if
   you need to SSH into the machine, this will not be used to access
   the web interface. 
7. Create the droplet. This will take ~5 mins to fully set up. Just
   because it gives an ip address does not mean it has finished
   installing. 
8. Access the droplet via `http://<IP-OF-DROPLET>/`. If it does not
   resolve it means it has not finished installing so just wait a few
   more minutes. 

### Build from Source
If the Docker images are not working you can build from source. This can either be done on a
local machine or by SSHing into a remote machine. This approach assumes some experience with
the command line. 
1. Clone this repository on the machine you want to run it on. 
2. CD into the root of the project. 
4. Build from source using this command. 
    ```sh
    docker compose -f docker-compose-build.yml up -d
    ```
5. Access via web browser. If on local machine, `http://localhost/`. If
   on a server `http://<IP-OF-SERVER>/`.
  
---
## Experiment Setup
Once the droplet is set up you can access `Rianú` via `http://<IP-OF-SERVER>/`. 

Before uploading videos you should set up your environment. Go to the
database page. 
1. Create a new experiment. You can choose what you want this to mean, but essentially an experiment is just a folder you use to group related videos. 
2. Add all the bioreactors youplan to use with information about post heights, tissue heights, etc. 

---
## Video Upload
Once you have your experiments and bioreactors set up you can start
uploading videos. Go to the upload page.

1. Select date recorded, which bioreactor it is in, and which
   experiment it is part of from dropdowns. 
2. Click to blue plus button to add as many tissues as are in the
   video you are uploading. 
3. For each tissue select which post it is on from the dropdown, and
   enter tissue number and type. 
4. Input the frequency this video was paced at, if spontaneous I
   usually just put 0. 
5. Add a video note, this will show up in the database to help you
   remember what is happening in this video. 
6. Upload a video and submit, you should be redirected to the database
   page. 
---
## Video Tracking
To track a video that has been uploaded go to the database page. 

1. Select the experiment from the `Videos` section below the
   experiments and bioreactors. 
2. The videos with greyed out `Analyze` buttons have not been tracked
   yet, find the one you want and click `Track`. You will be
   redirected to a new page. 
3. Calibrate the video. You have three options on how to do this. 
    1. **Calibration set**: If already calibrated a different video you
       can select that same calibration factor. 
    2. **Calibration factor**: If you analyzed the video in different
       software and have a calibration factor you can add it here. It should
       be in px/mm.
    3. **Calibration distance**: You can draw a box of a known
       length on the video to calibrate. First enter the known
       distance in mm, then draw a box on the frame. The diagonal of
       the box between the first and second click will be taken and
       used to calculate the calibration factor. 
4. Draw tissue widths. Draw a box for each tissue the length of its
   cross-section. The diagonal of the box between the first and second
   click will be used. 
5. Draw the post starting locations. Draw a box around each post. Here are a few important notes about his process. 
    1. The first click should be the top left corner and the second the bottom
        right, after the first click when you move your mouse you can see
        what the box looks like before confirming with the second click. 
    2. Tissues should be selected in the order they were entered on the
      upload. 
    3. Posts should be selected so the first post draw for each tissue
       corresponds to the left post entered in the bioreactor. 
6. Once the last post is selected the tracking will automatically
   start in the background. The `Analyze` button on the database page will turn blue once
   it has finished tracking and is ready for analysis. Tracking
   will usually take under 5 minutes, but it really depends on length
   of video, FPS, number of tissues being tracked, and how powerful
   the computer is.

<small>Video: Tracking Demo. Calibration box (black), tissue cross
sections (pink), and starting post locations (red) annotated. </small>
<figure class="video_container">
  <video controls="true" allowfullscreen="true" height="100%" width="100%" autoplay mute loop>
    <source src="tracking_demo.mov" type="video/mp4">
    <source src="tracking_demo.mov" type="video/ogg">
    <source src="tracking_demo.mov" type="video/webm">
  </video>
</figure>


---
## Tissue Analysis

1. You can either perform analysis by:
    - Each tissue: Below the videos in an experiment there is a dropdown
    with numbers. Select the number for the tissue you want to analyze.
    Analysis will be performed on that tissue for every video it is in.
    - Each video: Click analyze next to the video you want to analyze, it
    will perform analysis on all tissues present in that video
2. If there are no graphs just refresh the page. If the graphs do not automatically have peaks and points that means
   is couldn't find any peaks. Try these fixes. 
   1. Lower the threshold value
   2. Lower the minimum distance slider
3. Adjust the smoothing and point finding parameters until you agree
   with the points being found for each tissue. 
   - Polynomial (default: 3): Lower is smoother.
   - Window Size (default: 13): Higher is smoother.
   - Prominence (Default: .5): Decrease if it is not finding peaks (by
     amplitude).
   - Minimum Distance (default: 15): Increase to skip peaks (by time). 
   - Buffer (default: 15): Increase if finding basepoints of
     frontpoints before it should. 
   - Inverse (default: off): Inverts the raw data.
   - Detrend (default: off): Finds a removes a linear trend from the
     data. 
4. Once you are happy with all the points, click `Calculate` at the
   top of the page. A csv will be downloaded with twitch parameters
   for all tissues analyzed. 

---
## Experiment Archival
When an experiment has concluded a zip of that experiement can be
downloaded from the database page. The zip contains all videos,
bioreactor metadata, post location data, and twitch parameters. The experiment can then
be deleted off the server and the zip stored offline wherever the user
likes. If the would ever like to re-analyze that data they can go to
the upload page and upload the entire zip. 

---
