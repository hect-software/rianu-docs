---
weight: 60
#bookFlatSection: true
---

# Contributors
---
This page serves to acknowledge people who made significant
contributions as well as projects which this one relies upon.

---
## People
[`Jack F. Murphy`](https://jack.engineering) - Maintainer  
[`Brendan T. Murphy`](https://brendanmurphy.xyz) - Developer 

---
## Organizations
`Costa Lab` - This project was started and developed in the Costa Lab
at the Icahn School of Medicine in NYC. 

---
## Projects  
##### Frontend  
[`PlotlyJS`](https://github.com/plotly/plotly.js/) - Used to graph
data and allow user interaction.  
[`Svelte`](https://svelte.dev/) - Used for page design and user input.
  
##### Backend
[`OpenCV`](https://opencv.org/) - Used to track posts through frames.   
[`SciPy`](https://www.scipy.org/) - Used for data smoothing and
manipulation as well as peak finding.  
[`FastAPI`](https://github.com/tiangolo/fastapi) - Handles api requests.

##### Deployment
[`Docker`](https://docker.com) - Used for containerized deployment.

##### Documentation
[`hugo-book`](https://github.com/alex-shpak/hugo-book) - The theme
used for this documentation website.

---
