# Rianú

---

{{< hint warning >}}
**Repository**  
The canonical repository for this project is
[gitlab.com/hECT-Software/rianu](https://gitlab.com/hECT-Software/rianu) and
is also accessible at [10.17605/OSF.IO/YWCHZ](https://doi.org/10.17605/OSF.IO/YWCHZ).
Please keep all issues, feature requests, and pull requests on gitlab.
{{< /hint >}}

---

Rianú is an open-source software, released under the BSD 3-Clause
License, designed to more efficiently track and analyze engineered cardiac
tissues.

[![paper doi](https://img.shields.io/badge/paper%20doi-10.1016/j.cmpbup.2023.100107-blue)](https://doi.org/10.1016/j.cmpbup.2023.100107)
[![project doi](https://img.shields.io/badge/project%20doi-10.17605/OSF.IO/YWCHZ-blue)](https://doi.org/10.17605/OSF.IO/YWCHZ)
[![License](https://img.shields.io/badge/License-BSD_3--Clause-blue.svg)](https://gitlab.com/hect-software/rianu/-/blob/main/LICENSE)
[![pylint](https://hect-software.gitlab.io/rianu/badges/pylint.svg)](https://hect-software.gitlab.io/rianu/lint/)
[![code style](https://img.shields.io/badge/code%20style-black-000000.svg)](https://github.com/psf/black)
[![semantic-release](https://img.shields.io/badge/%20%20%F0%9F%93%A6%F0%9F%9A%80-semantic--release-e10079.svg)](https://github.com/semantic-release/semantic-release)

---

### Pronunciation

This project is named `rianú`, which means tracking in Irish.

<audio controls>
  <source src="https://storageapi.fleek.co/jack-alope-team-bucket/Rianú.mp3" type="audio/ogg">
  <source src="https://storageapi.fleek.co/jack-alope-team-bucket/Rianú.mp3" type="audio/mpeg">
Your browser does not support the audio element.
</audio>

> _Pronunciation kindly supplied by a native corkonian._

---

### Citation

If you use this software in research we kindly ask that you cite it as:

```bibtex
@article{MurphyRianu2023,
	doi = {10.1016/j.cmpbup.2023.100107},
	year = 2023,
	month = {5},
	publisher = {Elsevier {BV}},
	author = {Jack F. Murphy and Kevin D. Costa and Irene C. Turnbull},
	title = {Rian{\'{u}}: Multi-tissue tracking software for increased throughput of engineered cardiac tissue screening},
	journal = {Computer Methods and Programs in Biomedicine Update}
}
```

---

### Contact

If you have any questions about the use cases of this software, need
help using it, or want help adapting it to your specific applications
please open an issue on
[gitlab.com/hect-software/rianu](https://gitlab.com/hect-software/rianu/)
or email `jack@mrph.dev`.
